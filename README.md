# arkui_in_aciton



#### 介绍

[《ArkUI实战》](https://www.arkui.club)电子书问题反馈仓，如果您发现书中内容有错误，欢迎在本仓提 **Issue** 反馈，验证通过后，会立即更新电子书内容，电子书地址：[https://www.arkui.club](https://www.arkui.club)




#### 问题反馈

1. 点击 **Issue** 标签

2. 创建 **Issue** 输入反馈内容并提交

   

#### 问题处理
1.  每晚巡视 **Issue** 内容
2.  验证通过后，立即更新电子书

